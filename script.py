import bpy
import sys
# from io_scene_gltf2 import gltf2_export

argv = sys.argv
# argv = argv[argv.index("--") + 1:]
from addon_utils import check, paths, enable
from math import pi
from mathutils import Euler
enable('io_scene_gltf2')
bpy.ops.wm.read_factory_settings(use_empty=True)

print(argv[7])
bpy.ops.import_scene.obj(filepath=argv[7])

bpy.ops.mesh.separate(type='LOOSE')
bpy.ops.object.select_all(action='DESELECT')
for obj in bpy.context.scene.objects:
  if not obj.name.endswith('.001'):
    bpy.data.objects[obj.name].select = True
bpy.ops.object.delete() 

bpy.ops.object.select_all(action='SELECT')

for obj in bpy.context.scene.objects:
  bpy.context.scene.objects.active = obj
numberOfIteration=1
decimateRatio=0.69
modifierName='DecimateMod'


for i in range(0,numberOfIteration):
  objectList=bpy.data.objects
  for obj in objectList:
    if(obj.type=="MESH"):
      print("dec")
      for m in obj.modifiers:
        if(m.type=="DECIMATE"):
          obj.modifiers.remove(modifier=m)
      modifier=obj.modifiers.new(modifierName,'DECIMATE')
      modifier.ratio=1-decimateRatio*(i+1)
      modifier.use_collapse_triangulate=True
      bpy.context.scene.objects.active = obj
      
bpy.ops.object.modifier_apply(modifier=modifierName)
mat = bpy.data.materials.new(name="MaterialName")
activeObject = bpy.context.active_object

activeObject.data.materials.append(mat)
bpy.context.object.active_material.use_vertex_color_paint = True
bpy.context.object.active_material.use_shadeless = True


bpy.ops.transform.resize(value=(.02, .02, .02))
bpy.ops.transform.translate( value = ( -1.8, -0.6, 5 ) )

bpy.ops.wm.collada_import(filepath="/home/deploy/obj2glb3/background.dae")

enable('io_scene_gltf2')

bpy.ops.export_scene.glb(filepath=argv[8])

# bpy.ops.export_scene.glb(filepath="/Users/peek-mbp15/Downloads/test.glb")
##Cleans all decimate modifiers

# /Users/peek-mbp15/Downloads/2018081691534448447.obj