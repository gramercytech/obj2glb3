const puppeteer = require('puppeteer');
var FileReader = require('filereader')
  , reader = new FileReader()
  ;

var fs = require('file-system');


//get command line input and output
var args = process.argv.slice(2);
if (args.length !== 2 && args.length !== 3) {
  console.log('wrong number of arguments: node useragent.js input output');
  return null;
}
inputFile = args[0];
outputFile = args[1];
screenShot = args[2];
fileExt = args[0].split('.').pop();

if (fileExt != 'obj' && fileExt != 'ply'){
  console.log('invalid input, only supports obj and ply files');
  return null;
}



  (async () => {
  // const browser = await puppeteer.launch();
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      `--disable-web-security`,
    ]
  });
  const page = await browser.newPage();
  //generate glb
  if (screenShot) {
    page_url = 'http://localhost:8081/screenshot.html?url='
  } else if (fileExt == 'obj') {
    page_url = 'http://localhost:8081/obj2glb.html?url='
  } else if (fileExt == 'ply')
  {
    page_url = 'http://localhost:8081/obj2ply.html?url=';
  }
  await page.setViewport({width: 240, height: 240, deviceScaleFactor: 1})
  await page.goto(page_url+inputFile);
  ret_Val = await page.evaluate(async () => {
    return new Promise((resolve, reject) => {
      convertObj(resolve);
    });
  });
  if (screenShot) {
    await timeout(3000);
    await page.screenshot({path: outputFile});
  } else {
    fs.writeFileSync (outputFile, await ret_Val, { encoding: 'binary' });
  }
  console.log('Done');
  await browser.close();
})();


function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
};